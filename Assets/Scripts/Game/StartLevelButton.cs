﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class StartLevelButton : MonoBehaviour
{
    [SerializeField]
    private string _SceneName;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(MoveScene);
    }

    private void MoveScene() {
        UnityEngine.SceneManagement.SceneManager.LoadScene(_SceneName);
    }
}
