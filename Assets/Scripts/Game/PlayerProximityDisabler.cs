﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProximityDisabler : MonoBehaviour
{
    [SerializeField]
    private GameObject ControlledObject;
    [SerializeField]
    private float distance = 50;
    [SerializeField]
    private Renderer Renderer;
    // Update is called once per frame
    void Update()
    {
        ControlledObject.SetActive(Vector3.Distance(RelevantPoint(), PlayerTracker.Position) < distance);
    }

    private Vector3 RelevantPoint() {
        if (Renderer!=null) {
            return Renderer.bounds.ClosestPoint(PlayerTracker.Position);
        } else {
            return transform.position;
        }
    }
}
