﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTracker : MonoBehaviour
{
    public static Vector3 Position { get; private set; }

    // Update is called once per frame
    void Update()
    {
        Position = transform.position;
    }
}
