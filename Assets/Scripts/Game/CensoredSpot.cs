﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CensoredSpot : MonoBehaviour
{
    private static List<ICensoredSpot> activeSpots = new List<ICensoredSpot>();
    private static event Action<ICensoredSpot> _OnSpotEnabled;
    public static event Action<ICensoredSpot> OnSpotEnabled {
        add {
            _OnSpotEnabled += value;
            foreach (var spot in activeSpots) {
                value(spot);
            }
        }
        remove {
            _OnSpotEnabled -= value;
        }
    }
    public static event Action<ICensoredSpot> OnSpotDisabled;

    private CensoredSpotData spotData;

    [SerializeField]
    private float _Radius;
    // Start is called before the first frame update
    void Start()
    {
        spotData = new CensoredSpotData(_Radius);
        spotData.SetPosition(transform.position, transform.forward);
    }

    // Update is called once per frame
    void Update()
    {
        spotData.SetPosition(transform.position, transform.forward);
    }

    private void OnBecameVisible() {
        Debug.Log("New Spot visible.");
        activeSpots.Add(spotData);
        _OnSpotEnabled?.Invoke(spotData);
    }

    private void OnBecameInvisible() {
        activeSpots.Remove(spotData);
        OnSpotDisabled?.Invoke(spotData);
    }

    public interface ICensoredSpot {
        Vector3 WorldPos { get; }
        Vector3 WorldNormal { get; }
        float Radius { get; }
    }

    private class CensoredSpotData: ICensoredSpot {
        private Vector3 WorldPosition;
        private Vector3 WorldNormal;
        private float Radius;

        public CensoredSpotData(float radius) {
            Radius = radius;
        }

        public void SetPosition(Vector3 position, Vector3 normal) {
            WorldPosition = position;
            WorldNormal = normal;
        }

        Vector3 ICensoredSpot.WorldPos => WorldPosition;
        float ICensoredSpot.Radius => Radius;
        Vector3 ICensoredSpot.WorldNormal => WorldNormal;
    }
}
