﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CensoredPlates : MonoBehaviour {
    [SerializeField]
    private RectTransform PlatePrefab;
    [SerializeField]
    private Camera _Camera;


    private Stack<RectTransform> _PlateStash = new Stack<RectTransform>();

    private Dictionary<CensoredSpot.ICensoredSpot, RectTransform> _ActivePlates = new Dictionary<CensoredSpot.ICensoredSpot, RectTransform>();
    // Start is called before the first frame update
    void Start() {
        if (CensoreDisabler.CensoreDisabled) {
            Destroy(gameObject);
            return;
        }
        CensoredSpot.OnSpotEnabled += AddCensoredPlate;
        CensoredSpot.OnSpotDisabled += RemoveCensoredPlate;
    }

    // Update is called once per frame
    void Update() {
        foreach (var spot in _ActivePlates) {
            UpdatePlate(spot.Key, spot.Value);
        }
    }

    private void AddCensoredPlate(CensoredSpot.ICensoredSpot spot) {
        if (_ActivePlates.ContainsKey(spot))
            return;
        var newPlate = Fetch();
        newPlate.gameObject.SetActive(true);
        UpdatePlate(spot, newPlate);
        _ActivePlates.Add(spot, newPlate);
    }

    private void UpdatePlate(CensoredSpot.ICensoredSpot spot, RectTransform plate) {
        if (Vector3.Dot(spot.WorldNormal, _Camera.transform.forward) > 0.2) {
            plate.gameObject.SetActive(false);
        }
        else {
            plate.anchoredPosition = _Camera.WorldToScreenPoint(spot.WorldPos);
            var size = spot.Radius * 500 / Vector3.Distance(_Camera.transform.position, spot.WorldPos);
            plate.sizeDelta = new Vector2(size, size);
            plate.gameObject.SetActive(true);
        }
    }

    private void RemoveCensoredPlate(CensoredSpot.ICensoredSpot spot) {
        if (_ActivePlates.ContainsKey(spot) == false)
            return;
        var oldPlate = _ActivePlates[spot];
        if (oldPlate == null)
            return;
        oldPlate.gameObject.SetActive(false);
        _PlateStash.Push(oldPlate);
        _ActivePlates.Remove(spot);
    }

    private RectTransform Fetch() {
        while (_PlateStash.Count > 0) {
            var tmp = _PlateStash.Pop();
            if (tmp != null)
                return tmp;
        }
        return Instantiate(PlatePrefab, transform, false);
    }
}
