﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CtrlDisabled : MonoBehaviour {
    [SerializeField]
    private GameObject Controlled;
    // Update is called once per frame
    void Update() {
        Controlled.SetActive(Input.GetAxis("Control") < 0.2f);
    }
}
