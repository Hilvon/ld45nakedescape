﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CensoreDisabler : MonoBehaviour
{
    public static bool CensoreDisabled = false;

    [SerializeField]
    private Toggle Toggle;

    private void Start() {
        Toggle.isOn = CensoreDisabled;
        Toggle.onValueChanged.AddListener(UpdateDisableState);
        Debug.Log($"Initial Censore value {CensoreDisabled}");
    }

    public void UpdateDisableState(bool state) {
        Debug.Log($"Updating Censure state to {state}");
        CensoreDisabled = state;
    }
}
