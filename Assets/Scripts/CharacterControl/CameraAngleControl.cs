﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAngleControl : CameraAngleControlBase {

    protected override Vector3 _RotationAxis => Vector3.up;
}
