﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CameraAngleControlBase : MonoBehaviour
{
    [SerializeField]
    private string _InputAxtisName;
    [SerializeField]
    private float _RotationSpeed;

    protected abstract Vector3 _RotationAxis { get; }
    // Update is called once per frame
    protected virtual void Update()
    {
        float rotationAmount = Input.GetAxis(_InputAxtisName) * Time.deltaTime * _RotationSpeed;
        transform.Rotate(_RotationAxis, rotationAmount, Space.Self);
        PostProcess();
    }

    protected virtual void PostProcess() { }
}
