﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    [SerializeField]
    private CharacterController _Controller;

    [SerializeField]
    private float _Speed = 6;

    [SerializeField]
    private float _Acceleration = 10;

    public Vector3 WorldSpeed => _WorldSpeed;

    private Vector3 _WorldSpeed;
    private bool _IsFalling;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_IsFalling) {
            _WorldSpeed.y -= 9.8f * Time.deltaTime;
        }
        else {
            var intendedSpeed = transform.rotation * new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")) * _Speed;
            _WorldSpeed = Vector3.MoveTowards(_WorldSpeed, intendedSpeed, _Acceleration);
            _WorldSpeed.y = Input.GetAxis("Jump") > 0.2f ? 4 : 0;
        }
        var collisionsInfo = _Controller.Move(_WorldSpeed * Time.deltaTime);
        if ((collisionsInfo & CollisionFlags.CollidedSides) != 0) {
            if (Physics.CapsuleCast(_Controller.center-Vector3.down*_Controller.height/2,
                _Controller.center - Vector3.down * _Controller.height / 2,
                _Controller.radius,
                _WorldSpeed,
                out var hitinfo,
                _WorldSpeed.magnitude*Time.deltaTime)) {
                var stopSpeed = hitinfo.normal * Vector3.Dot(hitinfo.normal, _WorldSpeed);
                _WorldSpeed -= stopSpeed;
            }
        }
        _IsFalling = ((collisionsInfo & CollisionFlags.CollidedBelow) == 0);
        if ((collisionsInfo & CollisionFlags.CollidedAbove)!=0) {
            _WorldSpeed.y = 0;
        }
    }
}
