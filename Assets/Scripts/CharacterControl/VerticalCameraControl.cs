﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalCameraControl : CameraAngleControlBase {
    [SerializeField]
    private float _Min = -45;
    [SerializeField]
    private float _Max = 45;

    protected override Vector3 _RotationAxis => Vector3.right;
    protected override void PostProcess() {
        Vector3 locRot = transform.localEulerAngles;
        while (locRot.x>180) {
            locRot.x -= 360;
        }
        locRot.x = Mathf.Clamp(locRot.x, _Min, _Max);
        transform.localEulerAngles = locRot;
    }
}
