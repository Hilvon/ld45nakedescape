﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDistControl : MonoBehaviour {
    [SerializeField]
    private float IntendedOffset = 3;
    [SerializeField]
    private float ZoomSpeed = 5;

    [SerializeField]
    private float LerpSpeed = 5;

    [SerializeField]
    private float MaxZoom = 10;
    [SerializeField]
    private LayerMask CamObstacles;

    private void Update() {
        IntendedOffset = Mathf.Clamp(IntendedOffset + Input.mouseScrollDelta.y * Time.deltaTime * ZoomSpeed, 0, MaxZoom);

        var curOffset = Mathf.Lerp(-transform.localPosition.z, IntendedOffset, Time.deltaTime * LerpSpeed);
        if (Physics.SphereCast(transform.parent.position,0.2f, -transform.forward, out var hitinfo, curOffset, CamObstacles)) {
            curOffset = hitinfo.distance;
        }
        transform.localPosition = Vector3.back * curOffset;
    }
}
