﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimator : MonoBehaviour
{
    [SerializeField]
    private MovementController Mover;
    [SerializeField]
    private Animator Animator;

    private void Update() {
        var worldSpeed = Mover.WorldSpeed;
        worldSpeed.y = 0;
        if (worldSpeed.sqrMagnitude>0.1f) {
            transform.rotation = Quaternion.LookRotation(worldSpeed, Vector3.up);
            Animator.SetFloat("Velocity", worldSpeed.magnitude);
        } else {
            Animator.SetFloat("Velocity", 0);
        }
    }
}
