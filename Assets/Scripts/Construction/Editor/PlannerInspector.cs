﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ConstructionNode))]
public class PlannerInspector : Editor
{
    public override void OnInspectorGUI() {
        var prop = serializedObject.FindProperty("_PossibleOptions");
        for (int i = 0; i < prop.arraySize; i++) {
            var item = prop.GetArrayElementAtIndex(i).objectReferenceValue as ConstructionOption;
            if (item != null && GUILayout.Button(item.name)) {
                (serializedObject.targetObject as ConstructionNode)?.Construct(i);
            }
        }

        base.OnInspectorGUI();

    }
}
