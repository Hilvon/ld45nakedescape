﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstructionNode : MonoBehaviour
{
    [SerializeField]
    private ConstructionOption[] _PossibleOptions;

#if UNITY_EDITOR
    public void Construct(int option) {
        _PossibleOptions[option].PlaceItems(transform);
        GameObject.DestroyImmediate(gameObject);//  Destroy(gameObject);
    }
#endif
}
