﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneSeedRandomiser : MonoBehaviour
{
    public static System.Random RNG;
    [SerializeField]
    private string _Seed;

    private void Awake() {
        RNG = new System.Random(_Seed.GetHashCode());
    }
}
