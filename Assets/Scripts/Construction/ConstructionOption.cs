﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Constructor")]
public class ConstructionOption : ScriptableObject
{
    [SerializeField]
    private PosInfo[] BlocksToDrop;

#if UNITY_EDITOR
    public void PlaceItems(Transform positioning) {
        foreach (var item in BlocksToDrop) {
            var pos = positioning.TransformPoint(item.Offset);
            var rot = positioning.rotation * Quaternion.Euler(item.RotationEulers);
            var instance = UnityEditor.PrefabUtility.InstantiatePrefab(item.Token, positioning.parent) as GameObject;// Instantiate(item.Token, pos, rot);
            instance.transform.SetPositionAndRotation(pos, rot);
        }
    }
#endif

    [System.Serializable]
    private struct PosInfo {
        public Vector3 Offset;
        public Vector3 RotationEulers;
        public GameObject Token;
    }
}
