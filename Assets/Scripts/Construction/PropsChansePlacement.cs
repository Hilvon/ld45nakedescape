﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropsChansePlacement : MonoBehaviour
{
    [SerializeField]
    private float Chance = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        if (SceneSeedRandomiser.RNG.Next(1000)/1000f > Chance) {
            Destroy(gameObject);
        }
    }
    
}
